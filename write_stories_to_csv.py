import csv
import re
import requests
import sys

from bs4 import BeautifulSoup

def extract_stories(identifier):
    page = requests.get(f'https://www.navy.mil/listStories.asp?x={identifier}')
    parsed = BeautifulSoup(page.content, 'html.parser')
    links = []
    for div in parsed.find_all('div'):
        link = div.find('a')
        if link and 'story_id' in link.attrs['href']:
            date = re.search(r'\((.*?)\)', div.text).group()
            title = link.text.replace('\xa0','')
            url = 'https://www.navy.mil' + link.attrs['href']
            target = [title, url, date]
            if target not in links:
                links.append(target)
    return links

def write_stories_to_csv(identifier, links):
    with open(f'page-{identifier}.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        writer.writerow(['Title', 'URL', 'Date'])
        for link in links:
            writer.writerow(link)


if __name__ == "__main__":
    identifier = sys.argv[1]
    links = extract_stories(identifier)
    write_stories_to_csv(identifier, links)